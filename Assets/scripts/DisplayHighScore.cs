﻿
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayHighScore : MonoBehaviour {
    private TextMeshProUGUI TextMeshHighscore;
    

    private void Start()
    {
        TextMeshHighscore = gameObject.GetComponent<TextMeshProUGUI>();
    }
    // Update is called once per frame
    void Update () {
        TextMeshHighscore.text= Scores.Instance.Score.ToString();

    }
}
