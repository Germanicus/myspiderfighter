﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
/// <summary>
/// Game manager main class.
/// 
/// </summary>
public class EnemyLevels
{
    public string Enemy { get; set; }
    public int Level { get; set; }
    public float Firerate { get; set; }
    public float Velocity { get; set; }
}
public class ScoreEnemy
{
    public string Enemy { get; set; }
    public int scores { get; set; }
}
public class GameManager : MonoBehaviour {
    public static GameManager instance = null;
    public float PlayerBulletSpeed = 25f;
    public float EnemyBulletSpeed = 1f;
    //store List of spider game objects
    

    public List<EnemyLevels> EnemiesList = new List<EnemyLevels>() {
        new EnemyLevels{ Enemy="spider", Level=1000, Firerate=1.8f, Velocity= 3f},
        new EnemyLevels{ Enemy="spider", Level=1500, Firerate=1.5f, Velocity= 4f},
        new EnemyLevels{ Enemy="spider", Level=2000, Firerate=1f, Velocity= 6f},
        new EnemyLevels{ Enemy="spider", Level=2500, Firerate=0.777f, Velocity= 7f},
        new EnemyLevels{ Enemy="spider", Level=3500, Firerate=0.444f, Velocity= 9f},
        new EnemyLevels{ Enemy="spider", Level=4500, Firerate=0.444f, Velocity= 11f},
        new EnemyLevels{ Enemy="Tarantula", Level=1000, Firerate=1.5f, Velocity= 3f},
        new EnemyLevels{ Enemy="Tarantula", Level=1500, Firerate=1.2f, Velocity= 6f},
        new EnemyLevels{ Enemy="Tarantula", Level=2000, Firerate=0.888f, Velocity= 9f},
        new EnemyLevels{ Enemy="Tarantula", Level=2500, Firerate=0.6666f, Velocity= 11f},
        new EnemyLevels{ Enemy="Tarantula", Level=3500, Firerate=0.5555f, Velocity= 13f},
        new EnemyLevels{ Enemy="Tarantula", Level=4000, Firerate=0.3333f, Velocity= 14f},
        new EnemyLevels{ Enemy="MainBoss", Level=1000, Firerate=1.6f, Velocity= 3f},
        new EnemyLevels{ Enemy="MainBoss", Level=1500, Firerate=1.3f, Velocity= 4f},
        new EnemyLevels{ Enemy="MainBoss", Level=2000, Firerate=0.999f, Velocity= 6f},
        new EnemyLevels{ Enemy="MainBoss", Level=2500, Firerate=0.6666f, Velocity= 8f},
        new EnemyLevels{ Enemy="MainBoss", Level=3500, Firerate=0.555f, Velocity= 10f},
        new EnemyLevels{ Enemy="MainBoss", Level=4000, Firerate=0.333f, Velocity= 12f},
        new EnemyLevels{ Enemy="EnemyWave", Level=1500, Firerate=1.4f, Velocity= 3f},
        new EnemyLevels{ Enemy="EnemyWave", Level=2500, Firerate=1.2f, Velocity= 4f},
        new EnemyLevels{ Enemy="EnemyWave", Level=3000, Firerate=0.888f, Velocity= 6f},
        new EnemyLevels{ Enemy="EnemyWave", Level=3500, Firerate=0.555f, Velocity= 8f},
        new EnemyLevels{ Enemy="EnemyWave", Level=5000, Firerate=0.333f, Velocity= 10f},
        new EnemyLevels{ Enemy="LeftEnemyWave", Level=2000, Firerate=1.6f, Velocity= 2f},
        new EnemyLevels{ Enemy="LeftEnemyWave", Level=3500, Firerate=1.2f, Velocity= 4f},
        new EnemyLevels{ Enemy="LeftEnemyWave", Level=4000, Firerate=0.888f, Velocity= 6f},
        new EnemyLevels{ Enemy="LeftEnemyWave", Level=4500, Firerate=0.555f, Velocity= 8f},
        new EnemyLevels{ Enemy="LeftEnemyWave", Level=5000, Firerate=0.333f, Velocity= 10f},

        
    };

    public List<ScoreEnemy> scoreList = new List<ScoreEnemy>()
    {
        new ScoreEnemy{Enemy="spider", scores=100},
        new ScoreEnemy{Enemy="MainBoss", scores=300},
        new ScoreEnemy{Enemy="Tarantula", scores=200},
        new ScoreEnemy{Enemy="EnemyWave", scores=250},
        new ScoreEnemy{Enemy="LeftEnemyWave", scores=250}


    };
    
    public int EnemyLives = 2;
    public int playerLives = 4;
  

    bool gameHasEnded = false;
    bool secondLevel = false;
    
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    public void EndGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
          
            Invoke("GameOver", 0.5f);
        }
    }
    public void DrawHearts(GameObject[] lifes, int lives)
    {
        for (int i = 0; i < lifes.Length; i++)
        {
            if (lives >= i + 1)
            {
                lifes[i].SetActive(true);
            }
            else
                lifes[i].SetActive(false);
        }
    }

    ///game over scene managment
    void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    // next level scene menager
    void NextLevel(int number)
    {
        SceneManager.LoadScene("Level" + number.ToString());
    }

    
}



