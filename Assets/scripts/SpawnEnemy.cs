﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Enemy spawn class.
/// Class contains methods performs spawning enemies in spawn points.
/// </summary>
public class SpawnEnemy : MonoBehaviour {
    //strore Transform object for spawn  enemy 
    public Transform spawnPoint;
    //strore spider GameObject
    public GameObject spawnSpider;
    //store Trantula GameObject
    public GameObject Tarantula;
    //store int of maximum number of spawn enemy.
    public int spawnNumber;
    //store float of time to next spawn 
    private float nextSpawn;
    //store float of spawning rate
    public float spawnRate = 90.0f;
    //store bool for detecting if it should spawn
    public bool spawning=false;
    //store List of spider game objects
    public List<GameObject> spiderList = new List<GameObject>();
    //store List of Tarantula game objects
    public List<GameObject> tarantulaList = new List<GameObject>();
    //store List of wave enemies
    public List<GameObject> rightWaveList = new List<GameObject>();
    //store List of left wave enemies
    public List<GameObject> leftWaveList = new List<GameObject>();
    //store List of spawner enemy 
    public List<GameObject> leftWaveLis = new List<GameObject>();
    public List<GameObject> spawnerList = new List<GameObject>();
    // Use this for initialization
    // Use this for initialization


    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
       
            Spawn(spawnNumber);
       
        
        
        
	}

    // If list of spiders is smaller then int number, random spawn enemy objects  
    //also checks if there is no enemy on the scene, if it's true then spawn main boss enemy
    public void Spawn(int number)
    {
        if (Time.time >= nextSpawn)
        {
            if (spiderList.Count < number)
            {
                if (this.gameObject.tag == "MainBoss")
                {
                    if ((Random.Range(1, 15) % 5) == 0 )
                    {
                        EnemyInstantiate(spawnSpider, spiderList);
                    }

                    if ((Random.Range(1, 15) % 3) == 0)
                    {
                        EnemyInstantiate(Tarantula, tarantulaList);
                    }


                }
            }
            if (this.gameObject.tag == "spawnWave" && Scores.Instance.Score>= 1000 && GameObject.FindGameObjectWithTag("EnemyWave") == null)
            {

                EnemyInstantiate(spawnSpider,rightWaveList);


            }
            if (this.gameObject.tag == "LeftSpawnWave" && Scores.Instance.Score >= 2000 && GameObject.FindGameObjectWithTag("LeftEnemyWave") == null)
            {

                EnemyInstantiate(spawnSpider,leftWaveList);


            }
            



            nextSpawn = Time.time + spawnRate;
        }
        if (spawnerList.Count <= 0 && GameObject.FindGameObjectWithTag("spider")==null && GameObject.FindGameObjectWithTag("Tarantula") == null)
        {
            
            EnemyInstantiate(spawnSpider, spawnerList );
            
        }
        
        
        
        
        
               


    }

    //method instatiates enemies prefabs, adds enemies to list and set prefab as active on scene
    void EnemyInstantiate(GameObject EnemyType, List<GameObject> enemyList)
    {
        
            
            GameObject clone = Instantiate(EnemyType, spawnPoint.position, spawnPoint.rotation);
            GameObject spider = clone.GetComponent<GameObject>();
            enemyList.Add(spider);
            clone.SetActive(true);

        
    }

}
