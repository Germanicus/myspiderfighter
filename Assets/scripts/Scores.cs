﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
///Score class. 
/// Singleton design pattern. 
/// Class contains methods for performing incrementation Score. 
/// </summary>
public class Scores : MonoBehaviour {

    private int m_Score = 0;
    private static Scores m_Instance;
    public int Score
    {
        get { return m_Score; }
        set { m_Score = value; }
    }
    public static Scores Instance
    {
        get { return m_Instance; }
    }
    

    // Use this for initialization
    void Start () {
        if(m_Instance!=null)
        {
            Destroy(this.gameObject);
            return;
        }
        m_Instance = this;
	}
	
}
