﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Bullet main class.
/// Class contains methods which provides simple solutions 
/// to detect if bullet was collided with enemy and instantiate 
/// bullet prefabs.
/// </summary>
public class PlayerBullet : Bullets {

   
    
   

    public GameObject spawner;
    public GameObject DeathPraticle;
    public GameObject SpiderDeathParticle;
    public GameObject TarantulaDeathParticle;
    
  
    
   
   
   
    // Detects bullet collision with enemy objects
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("MainBoss"))
        {
           
            ParticlePrefabInstantiate(DeathPraticle, this.gameObject.transform);

            spawner.GetComponent<SpawnEnemy>().spawnerList.RemoveAt(0);
            
            EnemyCollide(collision.gameObject);
            GameManager.instance.EnemyLives--;
         
            if (GameManager.instance.EnemyLives == -1)
            {
                GameManager.instance.EnemyBulletSpeed += 5;
               


                GameManager.instance.EnemyLives = 3;
                Scores.Instance.Score += 500;
            }


        }
        if (collision.gameObject.CompareTag("spider"))
        {
           
            ParticlePrefabInstantiate(SpiderDeathParticle, this.gameObject.transform);
            EnemyCollide(collision.gameObject);

        }
        if(collision.gameObject.CompareTag("Tarantula"))
        {
            ParticlePrefabInstantiate(TarantulaDeathParticle, this.gameObject.transform);
            EnemyCollide(collision.gameObject);
        }
        if(collision.gameObject.CompareTag("EnemyWave"))
        {
            EnemyCollide(collision.gameObject);
        }
        if(collision.gameObject.CompareTag("LeftEnemyWave"))
        {
            EnemyCollide(collision.gameObject);
        }
        
        
        
        
        


    }
    
    void EnemyCollide(GameObject EnemyCollidedObject)
    {
        Destroy(this.gameObject);
        Destroy(EnemyCollidedObject);

    }
    void ParticlePrefabInstantiate(GameObject particle, Transform transform)
    {
        GameObject particleClone = Instantiate(particle, transform.position,transform.rotation);
        Destroy(particleClone, 1f);
    }
    // Use this for initialization
    void Start () {


        base.BulletMovement();
        
	}
    
	
	// Update is called once per frame
	void Update () {
       
    }
}
