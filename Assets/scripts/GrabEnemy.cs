﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabEnemy : MonoBehaviour {
    public Transform[] Fruit;
    public List<Transform> FruitList= new List<Transform>();
    public float MinDist = 1f;
    private bool grabbed;
   
    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        GrabFruit();
    }
    void GrabFruit()
    {
        for (int i = 0; i < 3; i++)
        {
            if (Vector3.Distance(transform.position, Fruit[i].position) <= MinDist)
            {


                Vector3.MoveTowards(transform.position, Fruit[i].position, Time.deltaTime);
                if (transform.position.x <= -3f && transform.position.y > 2.5f)
                {
                   
                    Fruit[i].SetParent(null);
                    grabbed = true;
                    

                }

                if (!grabbed )
                {
                    Debug.Log(transform.childCount);
                        FruitList.Add(Fruit[i]);
                        Fruit[i].SetParent(transform);
                        Fruit[i].position = new Vector2(transform.position.x, transform.position.y - 1.5f);
                        

                }
            }
        }
    }
}
