﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Main Player class.
/// Class contains methods performing movement and system of lives for player.
/// </summary>
public class PlayerController : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private Weapon fire;
    private bool immortal = true;
    private bool death = false;
    public Animator m_Animator;
    private GameManager systemLives;
    private int playerLives = 4;
    public GameObject[] lifes;
    public float speed;
    

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        fire = GetComponent<Weapon>();
        GameObject go= GameObject.Find("GameManager");
        m_Animator = GetComponent<Animator>();

    }

    private void Update()
    {
        PlayerShoot();
       

    }

    void FixedUpdate()
    {
        PlayerMovement();



    }
    //Detect if there is collision with enemy bullet's.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("enemyBullet"))
        {
            
            
            
            StartCoroutine(TakeDamage_Coroutine());
            
            Destroy(collision.gameObject);
            speed-=2.5f;
            GameManager.instance.PlayerBulletSpeed -= 6f;
            GameManager.instance.DrawHearts(lifes, GameManager.instance.playerLives);
            if (GameManager.instance.playerLives <= 0)
            {
                FindObjectOfType<GameManager>().EndGame();
            }



        }

    }
    IEnumerator TakeDamage_Coroutine()
    {
       
            death = true;
            GameManager.instance.playerLives--;
            
            m_Animator.SetBool("Death", death);
            immortal = true;
            yield return new WaitForSeconds(2f);
            death = false;
            m_Animator.SetBool("Death", death);



        
    }
    //initialization of player movement
    void PlayerMovement()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");

        Vector3 tempVect = new Vector3(moveHorizontal, 0,0);
        tempVect = tempVect.normalized * Mathf.Clamp(speed, 0, 10f) * Time.deltaTime;
        rb2d.MovePosition(rb2d.transform.position + tempVect);
    }
    

  

    //initialization of player shooting
    void PlayerShoot()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            fire.Shoot();

        }
    }
}

  
