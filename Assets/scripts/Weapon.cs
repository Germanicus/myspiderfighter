﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {
    //store position gunpoint 
    public Transform firepoint;
    //store game object of bullet prefab
    public GameObject bulletPrefab;
    //store List of all bullet prefabs on the scene
    private List<PlayerBullet> bulletsList = new List<PlayerBullet>();
    private List<EnemyBullet> EnemybulletsList = new List<EnemyBullet>();


    // Update is called once per frame
    void Update () {
      

        
       
	}
    
    public void Shoot()
    {
        GameObject clone;
        clone=  Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        PlayerBullet bullet = clone.GetComponent<PlayerBullet>();
        clone.SetActive(true);
        bullet.OnSpawn(this);
        bulletsList.Add(bullet);       
    }
    public void EnemyShoot()
    {
        GameObject clone;
        clone = Instantiate(bulletPrefab, firepoint.position, firepoint.rotation);
        EnemyBullet bullet = clone.GetComponent<EnemyBullet>();
        clone.SetActive(true);
        bullet.OnSpawn(this);
        EnemybulletsList.Add(bullet);
    }

    public void OnBulletDestroy(PlayerBullet bullet)
    {
       
        if (bulletsList.Contains(bullet) == true)
        {
            
            bulletsList.Remove(bullet);
            
        }
    }
 
}
