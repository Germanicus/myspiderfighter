﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullets : MonoBehaviour {
    
    public Rigidbody2D rb;
    private Weapon enemy_Weapon;

    public void OnSpawn(Weapon _enemyWeapon)
    {
        enemy_Weapon = _enemyWeapon;
        Destroy(this.gameObject, 1f);
    }



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public virtual void BulletMovement()
    {
        rb.velocity = transform.up * GameManager.instance.PlayerBulletSpeed;
    }
}
