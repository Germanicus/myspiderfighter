﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

///<summary>
///Main Enemy movement  class. 
///Contains all methods for perform
///simple enemy movement.
///</summary>

public class EnemyPatrol : MonoBehaviour {
    private float latestDirectionChangeTime;
    
    private readonly float directionChangeTime = 3f;
    //Vellocity of enemy game object
    public float characterVelocity = 2f;
    public GameObject[] EnemyLifes;
    public  float moveSpeed;
    public float frequency; //speed of sine movement
    public float magnitude; //Size of sine movemment
    private Vector3 axis;
    public GameObject DeathParticle;
    private Vector3 position; 

    //store Vector2 of current position
    public Vector2 speed;
    //store float rate of fire
    public float firerate = 0.111f;
    //store float to initialize next fire
    private float nextFire;
    
    private Vector2 movementDirection;
    private Vector2 movementPerSecond;
    private Weapon enemyFire;
    private GameManager enemyLevel; 
    /// store  array of postion numbers for Vector2 object 
    private float[] pos = new float[4];
    

    
    void Start()
    {

        position = transform.position;
        axis = transform.up;
        enemyFire = GetComponent<Weapon>();
        enemyLevel = GetComponent<GameManager>();
        
        pos[0] = -0.5f;
        pos[1] = 0.5f;
        pos[2] = -0.5f;
        pos[3] = 0.5f; 

        latestDirectionChangeTime = 0f;
        calculateNewMovementVector();
       




    }
    void Update()
    {
       
        //GameManager.instance.DrawHearts(EnemyLifes, GameManager.instance.EnemyLives);

        if (this.gameObject.tag == "EnemyWave")
        {
            
            WaveMovement();
        }
        if(this.gameObject.tag=="LeftEnemyWave")
        {
            LeftWaveMovement();
        }
        else
        {
            RandomMovement();
        }
        EnemyLevel();
       
        EnemyShoot();
      

    }


    void FixedUpdate()
    {

    }
    //Initialization of movement 
    void calculateNewMovementVector()
    {
       
        
        
         movementDirection = new Vector2(Random.Range(pos[0], pos[1]), Random.Range(pos[2], pos[3])).normalized;
        
        movementPerSecond = movementDirection * characterVelocity;
    }

    void EnemyLevel()
    {
        foreach (var enemy in GameManager.instance.EnemiesList)
        {

            if (Scores.Instance.Score >= enemy.Level && this.gameObject.tag == enemy.Enemy)
            {
                characterVelocity = enemy.Velocity;
                movementPerSecond= movementDirection * characterVelocity;
                firerate = enemy.Firerate;
            }
        }
    }  
   


    //Wave movement for enemies
    void  WaveMovement()
    {
       

            position += transform.right * Time.deltaTime * characterVelocity;
            transform.position = (position + axis * Mathf.Sin(Time.time * frequency) * magnitude);
            if(transform.position.x>10f)
            {
            Destroy(this.gameObject);
            }
        
        
       

    }
    //left wave movement for enemies
    void LeftWaveMovement()
    {
        position -= transform.right * Time.deltaTime * characterVelocity;
        transform.position = (position + axis * Mathf.Sin(Time.time * frequency) * magnitude);
        if(transform.position.x<-10f)
        {
            Destroy(this.gameObject);
        }
    }
    
    // Random movement for enemies 
    void RandomMovement()
    {
        speed = new Vector2(transform.position.x + (movementPerSecond.x * Time.deltaTime),
          transform.position.y + (movementPerSecond.y * Time.deltaTime));

        if (Time.time - latestDirectionChangeTime > directionChangeTime)
        {
            latestDirectionChangeTime = Time.time;
            calculateNewMovementVector();
        }

        if (transform.position.y > 4.0f)
        {

            pos[3] = -0.2f;
            calculateNewMovementVector();
        }
        if (transform.position.y < 0)
        {
            pos[3] = 0.5f;

        }
        if (transform.position.y < -4.0f)
        {

            pos[2] = 0.2f;
            calculateNewMovementVector();

        }
        if (transform.position.y > 0)
        {
            pos[2] = -0.5f;
        }

        if (transform.position.x > 11.0f)
        {
            pos[1] = -0.2f;
            calculateNewMovementVector();
        }
        if (transform.position.x < 0)
        {
            pos[1] = 0.5f;

        }
        if (transform.position.x < -8.0f)
        {
            pos[0] = 0.2f;
            calculateNewMovementVector();
        }
        if (transform.position.x > 0)
        {
            pos[0] = -0.5f;

        }
 


        transform.position = speed;


    }

    //Enemy shooting behaviour 
    void EnemyShoot()
    {
        if (Time.time >= nextFire)
        {

            enemyFire.EnemyShoot();
            nextFire = Time.time + firerate;
        }
    }




    //called when enemy GameObject is destroying
    public void OnDestroy()
    {
        foreach (var enemy in GameManager.instance.scoreList)
        {
            if (this.gameObject.tag == enemy.Enemy)
            {
                Scores.Instance.Score += enemy.scores;
            }
        }
       

    }
}
